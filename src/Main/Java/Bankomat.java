public class Bankomat {

    private int saldo;

    public void wplataPieniędzy(int wplata) {
        if (wplata>0)
            saldo = saldo + wplata;
    }
    public void wyplataPieniędzy(int wyplata) {
        if (wyplata<=saldo)
            saldo = saldo - wyplata;
        else {
            System.out.println("nie masz wystarczajacych srodkow na koncie");
        }

    }
    public int pokazSaldo(){
        System.out.println("masz na koncie "+ saldo);
        return saldo;
    }
}
