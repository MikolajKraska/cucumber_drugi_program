package io.cucumbe;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import static org.junit.Assert.*;

public class Stepdefs {


    @Given("Konto z saldem 200zl")
    public void konto_z_saldem(){
            System.out.println("Metoda Given konto z saldem");

            }

    @When("Uzytkownik probuje wyplacic 300zl")
    public void wyplata_z_konta(){
            System.out.println("Metoda When wyplata z konta");

            }

    @Then("Saldo nie zmienia sie i wynosi 200zl")
    public void sprawdzenie_salda(){
            System.out.println("Metoda Then sprawdzenie salda");

            }

}