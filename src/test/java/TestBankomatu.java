import org.junit.Assert;
import org.junit.Test;

public class TestBankomatu {
    @Test
    public void testWplaty(){
        Bankomat atm =new Bankomat();
        atm.wplataPieniędzy(200);
        Assert.assertTrue( atm.pokazSaldo()==200);
        atm.wplataPieniędzy(200);
        Assert.assertTrue( atm.pokazSaldo()==400);
    }

    @Test
    public void testWyplaty() {
        Bankomat atm =new Bankomat();
        atm.wplataPieniędzy(200);
        atm.wyplataPieniędzy(100);
        Assert.assertTrue( atm.pokazSaldo()==100);
        atm.wyplataPieniędzy(200);
        Assert.assertTrue( atm.pokazSaldo()==100);

    }


}